//
//  Constants.swift
//  FocusApp
//
//  Created by Shital on 25/06/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit

let kBaseURL = ""
let defaults = UserDefaults.standard
var userDefaults = UserDefaults.standard
let customOrangeColor  =   UIColor.red
/// My Profile color
let customWhiteColor = UIColor.init(red: 230, green: 230, blue: 230, alpha: 1)
let customLightWhiteColor = UIColor.init(red: 102, green: 102, blue: 102, alpha: 0.5)
let customYellowcolor = UIColor.init(red: 255, green: 226, blue: 0, alpha: 1)
let imageHome_white = UIImage(named: "home_white")
let imageTeens_white = UIImage(named: "teens_white")
let imageProfile_white = UIImage(named: "profile_white")
let imageSubcribe_white = UIImage(named: "subcribe_white")

let imageHome_yellow = UIImage(named: "home_yellow")
let imageTeens_yellow = UIImage(named: "teens_yellow")
let imageProfile_yellow = UIImage(named: "profile_yellow")
let imageSubcribe_yellow = UIImage(named: "subcribe_yellow")
