//
//  ChildCallLogsVC.swift
//  FocusApp
//
//  Created by Shital on 10/07/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit

class ChildCallLogsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
@IBOutlet weak var tableVW: UITableView!
@IBOutlet var searchController: UISearchController!
@IBOutlet var segmentedControl: UISegmentedControl!
     var shouldShowSearchResults = false
     var total_calls = ""
     var total_Msg = ""
     var arrayCall_data  = [[String:String]]()
     var arrayMsg_data  = [[String:String]]()
     var msg = ""
     var titleResponse = ""
     var call_type = ""
     var msg_type = ""
     var isCall = true
    var child_id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
         tableVW.tableFooterView = UIView()
         tableVW.layer.shadowColor = UIColor.black.cgColor
         tableVW.layer.shadowOpacity = 1
         tableVW.layer.shadowOffset = .zero
         tableVW.layer.shadowRadius = 10
        // configureSearchController()
        getChildCallLogAPI()
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: nil)

        
 
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if isCall == true
        {
            return arrayCall_data.count
        }
        else{
            
            return arrayMsg_data.count
    }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ChildlogCell"
        // var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                 for: indexPath) as! ChildlogCell
        if isCall == true{
        if arrayCall_data.count != 0 {
            
            cell.number?.text = arrayCall_data[0]["phone_number"]
            cell.date?.text = arrayCall_data[0]["created_at"]
            cell.time?.text = arrayCall_data[0]["call_duration"]
            call_type = arrayCall_data[0]["call_type"]!
            if call_type == "1" {
                cell.imgcall?.image = UIImage(named: "incoming")
                cell.call_type?.text = "incoming"
            }
            else{
                 cell.imgcall?.image = UIImage(named: "outgoing")
                cell.call_type?.text = "outgoing"
            }
        }
        }
        else
        {
            if arrayMsg_data.count != 0 {
                
            cell.number?.text = arrayMsg_data[indexPath.row]["phone_number"]
            cell.date?.text = arrayMsg_data[indexPath.row]["created_at"]
            cell.time?.text = "(\(total_Msg))"
            msg_type = arrayMsg_data[indexPath.row]["msg_type"]!
                if msg_type == "1" {
                    cell.imgcall?.image = UIImage(named: "msg_received")
                    cell.call_type?.text = "Received"
                }
                    
                else{
                     cell.imgcall?.image = UIImage(named: "msg_sent")
                     cell.call_type?.text = "Sent"
                }
            }
            }
   // cell.bgView?.bgShadow()
    return cell
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 80
       }
    
    
    @IBAction func indexChanged(_ sender: UISegmentedControl){
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            //textLabel.text = "First Segment Selected"
             isCall = true
            print("Call Segment Selected")
            getChildCallLogAPI()
        case 1:
            //textLabel.text = "Second Segment Selected"
            isCall = false
            print("msg Segment Selected")
            getChildMsgLogs()
            
        default:
            break
        }
    }
    /*
       func configureSearchController() {
           // Initialize and perform a minimum configuration to the search controller.
           searchController = UISearchController(searchResultsController: nil)
           searchController.searchResultsUpdater = self
           searchController.dimsBackgroundDuringPresentation = false
           searchController.searchBar.placeholder = "Search here..."
           searchController.searchBar.delegate = self
           searchController.searchBar.sizeToFit()
        
           // Place the search bar view to the tableview headerview.
          // tableVW.tableHeaderView = searchController.searchBar
       }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        shouldShowSearchResults = true
        tableVW.reloadData()
    }


    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        shouldShowSearchResults = false
        tableVW.reloadData()
    }
    func updateSearchResults(for searchController: UISearchController) {
           
       }*/
    // MARK: - API Call
    func getChildCallLogAPI(){
      //  let stringDob = formattedDateFromString(dateString: textFieldDob.text ?? "", withFormat: "yyyy-MM-dd")
        let url = URL(string: "http://14.140.184.228/secure_drive/PhoneLogs/getCallLogByUserId/" + child_id)
        let sv = UIViewController.displaySpinner(onView: self.view)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
        data, response, error in
        if error != nil {
        print(error!)
        return
        }
        print(response!)
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(responseString!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
              //  showAlertController(json["msg"], _title: nil)
                print(json)
                self.msg = json["msg"] as! String
                self.titleResponse = json["response"] as! String
                if (json["response"]as! String == "success"){
                    self.total_calls = json["data"]!["total_calls"] as! String
                    self.arrayCall_data =  json["data"]!["call_data"] as! [[String : String]]
                    print(self.arrayCall_data)
               // self.showAlertController(self.msg, _title: "")
                DispatchQueue.main.async
                    {
                      //AlertView.instance.showAlert(message: self.msg)
                        self.tableVW.reloadData()
                   UIViewController.removeSpinner(spinner: sv)
            }
                }
                else{
                    DispatchQueue.main.async
                    {
                    //self.msg = json["msg"] as! String
                 // AlertView.instance.showAlert(message: self.msg)
                    UIViewController.removeSpinner(spinner: sv)
                }
                }
            }
            catch {
                print("Couldn't parse json \(error)")
            }        }
        task.resume()
    }
    func getChildMsgLogs()
    {
        let url = URL(string: "http://14.140.184.228/secure_drive/PhoneLogs/getMsgLogByUserId/" + child_id)
        let sv = UIViewController.displaySpinner(onView: self.view)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
        data, response, error in
        if error != nil {
        print(error!)
        return
        }
        
        print(response!)
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(responseString!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
              //  showAlertController(json["msg"], _title: nil)
                print(json)
                self.msg = json["msg"] as! String
                self.titleResponse = json["response"] as! String
                if (json["response"]as! String == "success"){
                    self.total_Msg = json["data"]!["total_messages"] as! String
                    self.arrayMsg_data =  json["data"]!["msg_data"] as! [[String : String]]
                    print(self.arrayMsg_data)
               // self.showAlertController(self.msg, _title: "")
                DispatchQueue.main.async
                    {
                      //AlertView.instance.showAlert(message: self.msg)
                        self.tableVW.reloadData()
                   UIViewController.removeSpinner(spinner: sv)
            }
                }
                else{
                    DispatchQueue.main.async
                    {
                    //self.msg = json["msg"] as! String
                 // AlertView.instance.showAlert(message: self.msg)
                    UIViewController.removeSpinner(spinner: sv)
                }
                }
            }
            
           
            catch {
                print("Couldn't parse json \(error)")
            }        }
        task.resume()
    }
     @IBAction func btnback_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
}
}
