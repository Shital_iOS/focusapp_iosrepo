//
//  ParentHomeVC.swift
//  FocusApp
//
//  Created by Shital on 19/06/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit

class ParentHomeVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDescribe: UILabel!
    @IBOutlet var btnHome: UIButton!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var btnTeens: UIButton!
    @IBOutlet var btnSubcribe: UIButton!
     @IBOutlet var lblHome: UILabel!
    @IBOutlet var lblProfile: UILabel!
    @IBOutlet var lblTeens: UILabel!
    @IBOutlet var lblSubcribe: UILabel!
    @IBOutlet var btnAdddriver: UIButton!
    var strFullname = ""
    let imagePicker = UIImagePickerController()
  // private var customtab = FooterTabVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        footerTab()
        imagePicker.delegate = self
        imageView.layer.cornerRadius = self.imageView.frame.height/2
        lblDescribe.lineBreakMode = .byWordWrapping
        lblDescribe.numberOfLines = 0
        lblName.text = "Welcome " + strFullname
        btnAdddriver.layer.cornerRadius = 5
        
    }
    @IBAction func loadImageButtonTapped(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction  func btnTeens_Click(_ sender: UIButton) {
        let TeensHomeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TeensHomeVC") as? TeensHomeVC
        self.navigationController?.pushViewController(TeensHomeVC!, animated: true)
    }
    @IBAction  func btnSubcribe_Click(_ sender: UIButton) {
      
    let SubscriptionVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
 self.navigationController?.pushViewController(SubscriptionVC!, animated: true)
       
    }
    
    @IBAction  func btnProfile_Click(_ sender: UIButton) {
       
        let ParentProfileVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ParentProfileVC") as? ParentProfileVC
        self.navigationController?.pushViewController(ParentProfileVC!, animated: true)
    }
   
   
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFit
            imageView.image = pickedImage
           // uploadImg()
        }
        
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func footerTab(){

       // btnHome.setImage(UIImage(named:"home_yellow"), for: .normal)
        btnHome.setImage(UIImage(named: "home_yellow")?.withRenderingMode(.alwaysOriginal), for: .normal)

        lblHome.textColor = customYellowcolor
        btnHome.imageView?.tintColor = customYellowcolor
        
        btnTeens.setImage(UIImage(named:"teens_white"), for: .normal)
        lblTeens.textColor = customWhiteColor
        btnTeens.imageView?.tintColor = customWhiteColor
        
        btnProfile.setImage(UIImage(named:"profile_white"), for: .normal)
        lblProfile.textColor = customWhiteColor
        btnProfile.imageView?.tintColor = customWhiteColor
        
        btnSubcribe.setImage(UIImage(named:"subcribe_white"), for: .normal)
        lblSubcribe.textColor = customWhiteColor
        btnSubcribe.imageView?.tintColor = customWhiteColor
        
        
    }
}
