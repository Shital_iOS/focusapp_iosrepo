//
//  ViewController.swift
//  FocusApp
//
//  Created by Shital on 27/05/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate,AlertViewDelegate {
    
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnForgotPwd: UIButton!
    @IBOutlet weak var btnActivateChild: UIButton!
    var parent_id = ""
   var strfullname = ""
    var titleResonse = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true
        textFieldEmail.setRightPaddingPoints(20)
        textFieldEmail.setLeftPaddingPoints(40)
        textFieldPassword.setRightPaddingPoints(20)
        textFieldPassword.setLeftPaddingPoints(40)
        textFieldEmail.setPlaceHolderHashColor()
        textFieldPassword.setPlaceHolderHashColor()
        btnSignIn.layer.cornerRadius = 5
        btnActivateChild.layer.cornerRadius = 5
        addDoneButtonOnKeyboard()
        self.navigationController?.navigationBar.isHidden = true
        
    }
    @IBAction func btnSignInClick(){
        
        if self.textFieldEmail.text == nil ||  self.textFieldEmail.text == "" && self.textFieldPassword.text == nil ||  self.textFieldPassword.text == ""
        {
            self.showAlertController("Please Enter the Details", _title: "")
        }
       else
        {
          sendJson()
        }
    }
    
    func didTapOkButton(_ sender: UIButton) {
        if titleResonse == "success" {
          let ParentHomeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ParentHomeVC") as? ParentHomeVC
          self.navigationController?.pushViewController(ParentHomeVC!, animated: true)
            
        }
        else{
        AlertView.instance.removeFromSuperview()
        }
       
    }
    
    @IBAction func btnSignUpClick(){
        
        let ParentRegisterVC = storyboard?.instantiateViewController(withIdentifier: "ParentRegisterVC") as! ParentRegisterVC
        self.present(ParentRegisterVC, animated: true, completion: nil)
    }
     //MARK:- API Call

    
    func sendJson(){
        AlertView.instance.delegate = self
        let url = URL(string: "http://14.140.184.228/secure_drive/User/loginAction")
        let sv = UIViewController.displaySpinner(onView: self.view)

        let request = NSMutableURLRequest(url:url!)
        request.httpMethod = "POST"
        let postString = "email=\(textFieldEmail.text ?? "")&password=\(textFieldPassword.text ?? "")&token=\(userDefaults .object(forKey:"device_token")!)&deviceType=\("iOS")"
        print(postString)
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print(error!)
                return
            }
            print(response!)
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(responseString!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                //  showAlertController(json["msg"], _title: nil)
                print(json)
                let msg = json["msg"] as! String
                self.titleResonse = json["response"] as! String
                if (json["response"] as! String == "success"){
                self.strfullname = json["data"]!["full_name"] as! String
                //self.showAlertController(msg, _title: "")
               self.parent_id = json["data"]!["id"] as! String
                let fullNm = json["data"]!["full_name"] as! String
                let phone = json["data"]!["phone_number"] as! String
                let Dob = json["data"]!["dob"] as! String
              
                userDefaults.set(self.parent_id, forKey: "parent_id")
                userDefaults.set(fullNm, forKey: "full_name")
                userDefaults.set(phone, forKey: "phone_number")
                userDefaults.set(Dob, forKey: "dob")
                DispatchQueue.main.async
                    {
                         AlertView.instance.showAlert(message: msg)
                        
                UIViewController.removeSpinner(spinner: sv)
                
            }
                }
                else{
                    DispatchQueue.main.async
                    {
                         AlertView.instance.showAlert(message: msg)
                           UIViewController.removeSpinner(spinner: sv)
                           }
                }
            }
           
            catch {
                print("error")
        }
           
    }
         task.resume()
       
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect (x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        self.textFieldEmail.inputAccessoryView = doneToolbar
    }
    @objc func doneButtonAction()
    {
        self.textFieldEmail.resignFirstResponder()
    }
}
