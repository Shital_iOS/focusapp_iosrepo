//
//  RegisterChildVC.swift
//  FocusApp
//
//  Created by Shital on 09/06/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit

class RegisterChildVC: UIViewController,UITextFieldDelegate{
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldFullname: UITextField!
    @IBOutlet weak var textFieldPhoneNum: UITextField!
    @IBOutlet weak var textFieldDob: UITextField!
    var parent_id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
         
        // Do any additional setup after loading the view.
    }
    
    func RegisterChild()  {
          
        let url = URL(string: "http://14.140.184.228/secure_drive/User/registerChild")
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        let postString = "parent_id=\("3")&phone_number=\(textFieldPhoneNum.text ?? "")&dob=\(textFieldDob.text ?? "")"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print(error!)
                return
            }
            
            print(response!)
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(responseString!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                //  showAlertController(json["msg"], _title: nil)
                print(json)
                let msg = json["msg"] as! String
                self.parent_id = json["data"]!["id"] as! String
                self.showAlertController(msg, _title: "")
                self.ActivateChild()
            }
            catch {
                print("Couldn't parse json \(error)")
            }        }
        task.resume()
    }
    @IBAction func btnSignInClick(){
        
        RegisterChild()
    }
    func ActivateChild() {
      

        let url = URL(string: "http://14.140.184.228/secure_drive/User/activateChild")
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
       // let postString = "child_phone_number=\(textFieldPhoneNum.text ?? "")&parent_phone_number=\("456789123")&token=\(userDefaults .object(forKey:"device_token")!)&deviceType=\("iOS")"
        let postString = "child_phone_number=\(textFieldPhoneNum.text ?? "")&parent_phone_number=\("456789123")"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print(error!)
                return
            }
            
            print(response!)
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(responseString!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                //  showAlertController(json["msg"], _title: nil)
                print(json)
                //let msg = json["message"] as! String
                //self.showAlertController(msg, _title: "")
            }
            catch {
                print("Couldn't parse json \(error)")
            }        }
        task.resume()
    }
    func LoginChild()
    {
    let url = URL(string: "http://14.140.184.228/secure_drive/User/loginChild")
    
    let request = NSMutableURLRequest(url: url!)
    request.httpMethod = "POST"
    let postString = "child_phone_number=\(textFieldPhoneNum.text ?? "")&parent_phone_number=\("456789123")&token=\(userDefaults .object(forKey:"device_token")!)&deviceType=\("iOS")"
    request.httpBody = postString.data(using: String.Encoding.utf8)
    let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
    request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
    let task = URLSession.shared.dataTask(with: request as URLRequest) {
    data, response, error in
    
    if error != nil {
    print(error!)
    return
    }
    
    print(response!)
    
    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
    print(responseString!)
    do {
    let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
    //  showAlertController(json["msg"], _title: nil)
    print(json)
    //let msg = json["message"] as! String
    //self.showAlertController(msg, _title: "")
    }
    catch {
    print("Couldn't parse json \(error)")
    }        }
    task.resume()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignFirstResponder()
        return true
    }
}
