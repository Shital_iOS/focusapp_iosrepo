//
//  ParentRegisterVC.swift
//  FocusApp
//
//  Created by Shital on 04/06/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit

class ParentRegisterVC: UIViewController,UITextFieldDelegate,AlertViewDelegate{
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldFullname: UITextField!
    @IBOutlet weak var textFieldPhoneNum: UITextField!
    @IBOutlet weak var textFieldDob: UITextField!

    @IBOutlet weak var btnSignUp: UIButton!
    var msg = ""
    var titleResponse = ""
    var maxDate        = Date()
    var currentDate    = Date()
    var dateFrom  = Date()
    var dateTo    = Date()
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldFullname.setRightPaddingPoints(20)
        textFieldFullname.setLeftPaddingPoints(40)
        textFieldPhoneNum.setRightPaddingPoints(20)
        textFieldPhoneNum.setLeftPaddingPoints(40)
        textFieldDob.setRightPaddingPoints(20)
        textFieldDob.setLeftPaddingPoints(40)
        textFieldPassword.setRightPaddingPoints(20)
        textFieldPassword.setLeftPaddingPoints(40)
        textFieldFullname.setPlaceHolderHashColor()
        textFieldPassword.setPlaceHolderHashColor()
        textFieldPhoneNum.setPlaceHolderHashColor()
        textFieldDob.setPlaceHolderHashColor()
        btnSignUp.layer.cornerRadius = 5
        textFieldDob.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
         addDoneButtonOnKeyboard()
         AlertView.instance.delegate = self
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnSignUpClick(){
        
       RegisterParentAPI()
        // Create the alert controller
        
        // Create the actions
        
        
    }
     @IBAction func btnSignInClick(){
     AlertView.instance.removeFromSuperview()
        view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        
    }
    func didTapOkButton(_ sender: UIButton) {
         AlertView.instance.removeFromSuperview()
           if titleResponse == "success" {
              
           // self.navigationController?.popToRootViewController(animated: true)
            view.window?.rootViewController?.dismiss(animated: true, completion: nil)

           }
           else{
               AlertView.instance.removeFromSuperview()
           }
          
       }
    // MARK: - API Call
    func RegisterParentAPI(){
        let stringDob = formattedDateFromString(dateString: textFieldDob.text ?? "", withFormat: "yyyy-MM-dd")

        let url = URL(string: "http://14.140.184.228/secure_drive/User/registerParent")
        let sv = UIViewController.displaySpinner(onView: self.view)

        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
       // let postString = "email=\(textFieldEmail.text ?? "")&password=\(textFieldPassword.text ?? "")&full_name=\(textFieldFullname.text ?? "")&phone_number=\(textFieldPhoneNum.text ?? "")&dob=\(textFieldDob.text ?? "")"
        let postString = "password=\(textFieldPassword.text ?? "")&full_name=\(textFieldFullname.text ?? "")&phone_number=\(textFieldPhoneNum.text ?? "")&dob=\(stringDob ?? "")"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print(error!)
                return
            }
            
            print(response!)
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(responseString!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
              //  showAlertController(json["msg"], _title: nil)
                print(json)
                 self.msg = json["msg"] as! String
                self.titleResponse = json["response"] as! String
                if (json["response"] as! String == "success"){
               
               // self.showAlertController(self.msg, _title: "")
                DispatchQueue.main.async
                    {
                      AlertView.instance.showAlert(message: self.msg)
                   UIViewController.removeSpinner(spinner: sv)
            }
                }
                else{
                    DispatchQueue.main.async
                    {
                    //self.msg = json["msg"] as! String
                  AlertView.instance.showAlert(message: self.msg)
                    UIViewController.removeSpinner(spinner: sv)
                }
                }
            }
            
           
            catch {
                print("Couldn't parse json \(error)")
            }        }
        task.resume()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldDob {
            
    }
}
    @objc func tapDone() {
        if let datePicker = textFieldDob.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .medium // 2-3
            

           textFieldDob.text = dateformatter.string(from: datePicker.date) //2-4
        }
       textFieldDob.resignFirstResponder() // 2-5
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect (x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))

        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        self.textFieldPhoneNum.inputAccessoryView = doneToolbar
    }
    @objc func doneButtonAction()
    {
        self.textFieldPhoneNum.resignFirstResponder()
    }
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {

        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MM/dd/yyyy"

        if let date = inputFormatter.date(from: dateString) {

            let outputFormatter = DateFormatter()
          outputFormatter.dateFormat = format

            return outputFormatter.string(from: date)
        }

        return nil
    }
}
