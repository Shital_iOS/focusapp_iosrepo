//
//  SubscriptionVC.swift
//  FocusApp
//
//  Created by Shital on 25/06/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit


class SubscriptionVC: UIViewController {
    @IBOutlet var btnHome: UIButton!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var btnTeens: UIButton!
    @IBOutlet var btnSubcribe: UIButton!
    @IBOutlet var lblHome: UILabel!
    @IBOutlet var lblProfile: UILabel!
    @IBOutlet var lblTeens: UILabel!
    @IBOutlet var lblSubcribe: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
          footerTab()
    }
    
    func footerTab(){
        btnHome.setImage(UIImage(named: "home_white"), for: .normal)
        lblHome.textColor = customWhiteColor
        btnHome.imageView?.tintColor = customWhiteColor
        
        btnTeens.setImage(UIImage(named: "teens_white"), for: .normal)
        lblTeens.textColor = customWhiteColor
        btnTeens.imageView?.tintColor = customWhiteColor
        btnProfile.setImage(UIImage(named: "profile_white"), for: .normal)
        lblProfile.textColor = customWhiteColor
        btnProfile.imageView?.tintColor = customWhiteColor
        
        //btnSubcribe.setImage(UIImage(named: "subscribe_yellow"), for: .normal)
        btnSubcribe.setImage(UIImage(named: "subscribe_yellow")?.withRenderingMode(.alwaysOriginal), for: .normal)
        lblSubcribe.textColor = customYellowcolor
        btnSubcribe.imageView?.tintColor = customYellowcolor
        
        
    }
    @IBAction func btnHome_Click(_ sender: Any) {
        let ParentHomeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ParentHomeVC") as? ParentHomeVC
        self.navigationController?.pushViewController(ParentHomeVC!, animated: true)
    }
    @IBAction  func btnProfile_Click(_ sender: UIButton) {
         let ParentProfileVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ParentProfileVC") as? ParentProfileVC
               self.navigationController?.pushViewController(ParentProfileVC!, animated: true)
    }
    @IBAction func btnTeens_Click(_ sender: Any) {
         let TeensHomeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TeensHomeVC") as? TeensHomeVC
               self.navigationController?.pushViewController(TeensHomeVC!, animated: true)
    }
}
