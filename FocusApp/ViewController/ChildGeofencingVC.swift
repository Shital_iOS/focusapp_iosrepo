//
//  ChildGeofencingVC.swift
//  FocusApp
//
//  Created by Shital on 14/07/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit
import GoogleMaps

class ChildGeofencingVC: UIViewController, GMSMapViewDelegate {
 @IBOutlet weak var mapView: GMSMapView!
private let locationManager = CLLocationManager()

    override func viewDidLoad() {
              super.viewDidLoad()
              locationManager.delegate = self
              locationManager.requestWhenInUseAuthorization()
              mapView.delegate = self
            let camera = GMSCameraPosition.camera(withLatitude: 16.9783622, longitude: 81.7859619, zoom: 6.0)
            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            view = mapView
            
              // Creates a marker in the center of the map.
              let marker = GMSMarker()
              marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
              marker.title = "Sydney"
              marker.snippet = "Australia"
              marker.map = mapView
        }
}
// MARK: - CLLocationManagerDelegate
//1
extension ChildGeofencingVC: CLLocationManagerDelegate {
  // 2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      guard status == .authorizedWhenInUse else {
        return
      }
      
      locationManager.startUpdatingLocation()
      mapView.isMyLocationEnabled = true
      mapView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      guard let location = locations.first else {
        return
      }
      
      mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
      locationManager.stopUpdatingLocation()
   //   fetchNearbyPlaces(coordinate: location.coordinate)
    }
  }
