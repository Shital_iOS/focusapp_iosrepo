//
//  ParentProfileVC.swift
//  FocusApp
//
//  Created by Shital on 22/06/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit

class ParentProfileVC: UIViewController,UITextFieldDelegate,AlertViewDelegate {
    @IBOutlet weak var textFieldDrivername: UITextField!
    @IBOutlet weak var textFieldPhoneNum: UITextField!
    @IBOutlet weak var textFieldDob: UITextField!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblMyprofile: UILabel!
    @IBOutlet weak var btnAddDriver: UIButton!
    @IBOutlet weak var btnMyprofile: UIButton!
    //Add child
    @IBOutlet var btnHome: UIButton!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var btnTeens: UIButton!
    @IBOutlet var btnSubcribe: UIButton!
    @IBOutlet var lblHome: UILabel!
    @IBOutlet var lblProfile: UILabel!
    @IBOutlet var lblTeens: UILabel!
    @IBOutlet var lblSubcribe: UILabel!
     @IBOutlet var viewAddChild: UIView!
    @IBOutlet var AddnewDriver: UIButton!
    //// profileView
    @IBOutlet weak var textFieldFullname: UITextField!
    @IBOutlet weak var textFieldPhoneNumProfile: UITextField!
    @IBOutlet weak var textFieldDobProfile: UITextField!
    @IBOutlet var btnUpdateProfile: UIButton!
    @IBOutlet var btnLogout: UIButton!
     @IBOutlet var viewProfile: UIView!
    var parent_id = ""
    var parent_PhoneNum = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldDrivername.setRightPaddingPoints(20)
        textFieldDrivername.setLeftPaddingPoints(40)
        textFieldPhoneNum.setRightPaddingPoints(20)
        textFieldPhoneNum.setLeftPaddingPoints(40)
        textFieldDob.setRightPaddingPoints(20)
        textFieldDob.setLeftPaddingPoints(40)
        textFieldDob.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
        
        textFieldDrivername.setPlaceHolderHashColor()
        textFieldPhoneNum.setPlaceHolderHashColor()
        textFieldDob.setPlaceHolderHashColor()
        // Do any additional setup after loading the view.
        lblMyprofile.isHidden = false
        lblDriverName.isHidden = true
        btnMyprofile.setTitleColor(customWhiteColor, for: .normal)
        btnAddDriver.setTitleColor(customLightWhiteColor, for: .normal)
        footerTab()
        //// profile View
        textFieldFullname.setRightPaddingPoints(20)
        textFieldFullname.setLeftPaddingPoints(40)
        textFieldPhoneNumProfile.setRightPaddingPoints(20)
        textFieldPhoneNumProfile.setLeftPaddingPoints(40)
        textFieldDobProfile.setRightPaddingPoints(20)
        textFieldDobProfile.setLeftPaddingPoints(40)
        viewAddChild.isHidden = true
        viewProfile.isHidden = false
        parent_id = userDefaults.value(forKey: "parent_id") as! String
        textFieldFullname.text = userDefaults.value(forKey: "full_name") as? String
        textFieldPhoneNumProfile.text = userDefaults.value(forKey: "phone_number") as? String
        textFieldFullname.setPlaceHolderHashColor()
        textFieldPhoneNumProfile.setPlaceHolderHashColor()
        textFieldDobProfile.setPlaceHolderHashColor()
        textFieldDobProfile.text = userDefaults.value(forKey: "dob") as? String
        parent_PhoneNum = userDefaults.value(forKey: "phone_number") as! String
        btnUpdateProfile.layer.cornerRadius = 5
        AddnewDriver.layer.cornerRadius = 5
        btnLogout.layer.cornerRadius = 5
        addDoneButtonOnKeyboard()
        btnLogout.isHidden = false
        AlertView.instance.delegate = self
    }
    

    @IBAction func btnAddDriver_Click(_ sender: Any) {
        lblMyprofile.isHidden = true
        lblDriverName.isHidden = false
        btnAddDriver.setTitleColor(customWhiteColor, for: .normal)
        btnMyprofile.setTitleColor(customLightWhiteColor, for: .normal)
        viewAddChild.isHidden = false
        viewProfile.isHidden = true
        btnLogout.isHidden = true
    }
     @IBAction func btnAddnewDriver_Click(_ sender: Any) {
         RegisterChild()
    }
    @IBAction func btnMyProfile_Click(_ sender: Any) {
        lblMyprofile.isHidden = false
        lblDriverName.isHidden = true
        btnMyprofile.setTitleColor(customWhiteColor, for: .normal)
        btnAddDriver.setTitleColor(customLightWhiteColor, for: .normal)
        viewAddChild.isHidden = true
        viewProfile.isHidden = false
        btnLogout.isHidden = false
        
        textFieldDrivername.text = ""
        textFieldDob.text = ""
        textFieldPhoneNum.text = ""
    }
     @IBAction func btnHome_Click(_ sender: Any) {
        let ParentHomeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ParentHomeVC") as? ParentHomeVC
               self.navigationController?.pushViewController(ParentHomeVC!, animated: true)
    }
    @IBAction func btnTeens_Click(_ sender: Any) {
        
        let TeensHomeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TeensHomeVC") as? TeensHomeVC
        self.navigationController?.pushViewController(TeensHomeVC!, animated: true)
    }
    
    @IBAction func btnSubcribe_Click(_ sender: Any) {
         let SubscriptionVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
        self.navigationController?.pushViewController(SubscriptionVC!, animated: true)
    }
    @IBAction func btnLogout_Click(_ sender: Any) {
       
        let LoginVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as? ViewController
        self.navigationController?.pushViewController(LoginVC!, animated: true)
    }
    @objc func tapDone() {
        if let datePicker = textFieldDob.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .medium // 2-3
            
            
            textFieldDob.text = dateformatter.string(from: datePicker.date) //2-4
        }
        textFieldDob.resignFirstResponder() // 2-5
    }
   func didTapOkButton(_ sender: UIButton) {
    AlertView.instance.removeFromSuperview()
    }
    func RegisterChild()  {
        let stringDob = formattedDateFromString(dateString: textFieldDob.text ?? "", withFormat: "yyyy-MM-dd")
        let url = URL(string: "http://14.140.184.228/secure_drive/User/registerChild")
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        let postString = "parent_id=\(parent_id)&phone_number=\(textFieldPhoneNum.text ?? "")&dob=\(stringDob ?? "")"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print(error!)
                return
            }
            
            print(response!)
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(responseString!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                //  showAlertController(json["msg"], _title: nil)
                print(json)
                
                let msg = json["msg"] as! String
                if (json["response"] as! String == "success"){
               // self.parent_id = json["data"]!["id"] as! String
               // self.showAlertController(msg, _title: "")
                self.genrateChildtoken()
               // self.ActivateChild()
            }
                else{
                    //self.showAlertController(msg, _title: "")
                    DispatchQueue.main.async
                                      {
                   AlertView.instance.showAlert(message: msg)
                }
                }
            }
            catch {
                print("Couldn't parse json \(error)")
            }        }
        task.resume()
    }
    func genrateChildtoken()  {
        
        let child_phone = textFieldPhoneNum.text
        let url = URL(string: "http://14.140.184.228/secure_drive/User/generateChildLogin")
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        let postString = "parent_phone_number=\(parent_PhoneNum)&child_phone_number=\(child_phone ?? "")"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print(error!)
                return
            }
            
            print(response!)
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(responseString!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                //  showAlertController(json["msg"], _title: nil)
                print(json)
                let msg = json["msg"] as! String
                
                let code = json["code"] as! String
               
                // self.parent_id = json["data"]!["id"] as! String
              //  self.showAlertController(msg + "\n Code - \(code) " , _title: "")
                DispatchQueue.main.async
                                  {
                 AlertView.instance.showAlert( message: msg + "\n Code - \(code) ")
                // self.ActivateChild()
            }
            }
            catch {
                print("Couldn't parse json \(error)")
            }        }
        task.resume()
    }
    func footerTab(){
        btnHome.setImage(UIImage(named: "home_white"), for: .normal)
        lblHome.textColor = customWhiteColor
        btnHome.imageView?.tintColor = customWhiteColor
        
        btnTeens.setImage(UIImage(named: "teens_white"), for: .normal)
        lblTeens.textColor = customWhiteColor
        btnTeens.imageView?.tintColor = customWhiteColor
        
       // btnProfile.setImage(UIImage(named: "profile_yellow"), for: .normal)
        btnProfile.setImage(UIImage(named: "profile_yellow")?.withRenderingMode(.alwaysOriginal), for: .normal)
        lblProfile.textColor = customYellowcolor
        btnProfile.imageView?.tintColor = customYellowcolor
        
        btnSubcribe.setImage(UIImage(named: "subcribe_white"), for: .normal)
        lblSubcribe.textColor = customWhiteColor
        btnSubcribe.imageView?.tintColor = customWhiteColor
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect (x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        self.textFieldPhoneNum.inputAccessoryView = doneToolbar
    }
    @objc func doneButtonAction()
    {
        self.textFieldPhoneNum.resignFirstResponder()
    }
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {

        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MM/dd/yyyy"

        if let date = inputFormatter.date(from: dateString) {

            let outputFormatter = DateFormatter()
          outputFormatter.dateFormat = format

            return outputFormatter.string(from: date)
        }

        return nil
    }
}
