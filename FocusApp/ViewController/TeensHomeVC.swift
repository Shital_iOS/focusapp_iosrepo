//
//  TeensHomeVC.swift
//  FocusApp
//
//  Created by Shital on 25/06/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit

class TeensHomeVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var btnHome: UIButton!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var btnTeens: UIButton!
    @IBOutlet var btnSubcribe: UIButton!
    @IBOutlet var lblHome: UILabel!
    @IBOutlet var lblProfile: UILabel!
    @IBOutlet var lblTeens: UILabel!
    @IBOutlet var lblSubcribe: UILabel!
    var parent_id = ""
    var dataArray = [[String:Any]]()
    var Chils_id = ""
    @IBOutlet weak var tableVW: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        footerTab()
        parent_id = userDefaults.value(forKey: "parent_id") as! String
        GetChildList()
        tableVW.tableFooterView = UIView()
                tableVW.layer.shadowColor = UIColor.black.cgColor
                tableVW.layer.shadowOpacity = 1
                tableVW.layer.shadowOffset = .zero
                tableVW.layer.shadowRadius = 10
    }
    
    func footerTab(){
        btnHome.setImage(UIImage(named: "home_white"), for: .normal)
        lblHome.textColor = customWhiteColor
        btnHome.imageView?.tintColor = customWhiteColor
        
       // btnTeens.setImage(UIImage(named: "teens_yellow"), for: .normal)
        btnTeens.setImage(UIImage(named: "teens_yellow")?.withRenderingMode(.alwaysOriginal), for: .normal)
        lblTeens.textColor = customYellowcolor
        btnTeens.imageView?.tintColor = customYellowcolor
        
        btnProfile.setImage(UIImage(named: "profile_white"), for: .normal)
        lblProfile.textColor = customWhiteColor
        btnProfile.imageView?.tintColor = customWhiteColor
        
        btnSubcribe.setImage(UIImage(named: "subcribe_white"), for: .normal)
        lblSubcribe.textColor = customWhiteColor
        btnSubcribe.imageView?.tintColor = customWhiteColor
        
        
    }
     @IBAction func btnCallLogs_Click(_ sender: Any) {
       
    }
    @IBAction func btnLocationBoundry_Click(_ sender: Any) {
       let ChildGeofencingVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChildGeofencingVC") as? ChildGeofencingVC
       self.navigationController?.pushViewController(ChildGeofencingVC!, animated: true)
    }
    @IBAction func btnHome_Click(_ sender: Any) {
        let ParentHomeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ParentHomeVC") as? ParentHomeVC
                      self.navigationController?.pushViewController(ParentHomeVC!, animated: true)
    }
    @IBAction func btnSubcribe_Click(_ sender: Any) {
        let SubscriptionVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
        self.navigationController?.pushViewController(SubscriptionVC!, animated: true)
    }
    @IBAction  func btnProfile_Click(_ sender: UIButton) {
        let ParentProfileVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ParentProfileVC") as? ParentProfileVC
        self.navigationController?.pushViewController(ParentProfileVC!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return dataArray.count
       }
       func numberOfSections(in tableView: UITableView) -> Int {
            return 1
          
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cellIdentifier = "TeensdetailCell"
           // var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
           let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                    for: indexPath) as! TeensdetailCell
           if dataArray.count != 0 {
               
            cell.phone_num?.text = dataArray[indexPath.row]["phone_number"] as? String
            cell.full_name?.text = dataArray[indexPath.row]["created_at"] as? String
            cell.imgcall?.image = UIImage (named: "profile")
           }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
        let ChildCallLogsVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChildCallLogsVC") as? ChildCallLogsVC
           ChildCallLogsVC!.child_id =  dataArray[indexPath.row]["id"] as! String
                      self.navigationController?.pushViewController(ChildCallLogsVC!, animated: true)
    }
    func GetChildList()
    {
    let url = URL(string: "http://14.140.184.228/secure_drive/user/getChildrenByParentId/" + parent_id)
    let sv = UIViewController.displaySpinner(onView: self.view)
    let request = NSMutableURLRequest(url: url!)
    request.httpMethod = "GET"
    let accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVJZCI6IjU1NTU1Iiwicm9sZSI6ImFkbWluIiwidGltZVN0YW1wIjoiMjAyMC0wNi0wNCAwMzo1Mzo1NyJ9.Emk3JY3XSa2RRj7lqyyrPP9fpenFt1dAvd0tlV-yub4"
    request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
    let task = URLSession.shared.dataTask(with: request as URLRequest) {
    data, response, error in
    
    if error != nil {
    print(error!)
    return
    }
    
    print(response!)
    
    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
    print(responseString!)
    do {
    let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
    //  showAlertController(json["msg"], _title: nil)
    print(json)
   let msg = json["msg"] as! String
  self.dataArray = json["data"] as! [[String : Any]]
  print(self.dataArray)
       
    //self.showAlertController(msg, _title: "")
        DispatchQueue.main.async
                {
                  //AlertView.instance.showAlert(message: self.msg)
                    self.tableVW.reloadData()
               UIViewController.removeSpinner(spinner: sv)
        }
    }
    catch {
    print("Couldn't parse json \(error)")
    }
        
        
        }
         UIViewController.removeSpinner(spinner: sv)
    task.resume()
    }
}
