//
//  FooterTabVC.swift
//  FocusApp
//
//  Created by Shital on 23/06/20.
//  Copyright © 2020 Kastech. All rights reserved.
//

import UIKit
protocol FootertabDelegate: class {
    func didTapHomeButton(_ sender: UIButton)
    func didTapTeensButton(_ sender: UIButton)
    func didTapProfileButton(_ sender: UIButton)
    func didTapSubscribeButton(_ sender: UIButton)
}

class FooterTabVC: UIView {
    @IBOutlet var btnHome: UIButton!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var btnTeens: UIButton!
    @IBOutlet var btnSubscribe: UIButton!
    @IBOutlet var lblHome: UILabel!
    @IBOutlet var lblProfile: UILabel!
    @IBOutlet var lblTeens: UILabel!
    @IBOutlet var lblSubscribe: UILabel!
    @IBOutlet var ContentView: UIView!
    
 weak var delegate: FootertabDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func setup() {
        ContentView = loadViewFromNib()
        ContentView.frame = bounds
        ContentView.autoresizingMask = .flexibleWidth
        ContentView.autoresizingMask = .flexibleHeight
       // btnHome.setImage(UIImage(named: "home_white"), for: .normal)
     //   btnTeens.setImage(UIImage(named: "teens_white"), for: .normal)
      //  btnProfile.setImage(UIImage(named: "profile_white"), for: .normal)
        //btnSubscribe.setImage(UIImage(named: "subcribe_white"), for: .normal)
        addSubview(ContentView)

    }
    
    func loadViewFromNib() -> UIView {
       return UINib(nibName: "FooterTabVC", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "FooterTabVC", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
 
    
    
    @IBAction func btnHome_Click(_ sender: UIButton) {
        delegate?.didTapHomeButton(sender)
    }
    @IBAction  func btnTeens_Click(_ sender: UIButton) {
        delegate?.didTapTeensButton(sender)
    }
    @IBAction  func btnSubcribe_Click(_ sender: UIButton) {
        delegate?.didTapSubscribeButton(sender)
    }
    @IBAction  func btnProfile_Click(_ sender: UIButton) {
        delegate?.didTapProfileButton(sender)
    }
    
    
    
}
