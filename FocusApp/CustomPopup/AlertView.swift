//
//  AlertView.swift
//  CustomAlert
//
//  Created by SHUBHAM AGARWAL on 31/12/18.
//  Copyright © 2018 SHUBHAM AGARWAL. All rights reserved.
//

import Foundation
import UIKit
protocol AlertViewDelegate: class {
func didTapOkButton(_ sender: UIButton)
}
class AlertView: UIView {
    
    static let instance = AlertView()
    
    @IBOutlet var parentView: UIView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
     weak var delegate: AlertViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("AlertView", owner: self, options: nil)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
//        img.layer.cornerRadius = 30
//        img.layer.borderColor = UIColor.white.cgColor
//        img.layer.borderWidth = 2
        
        alertView.layer.cornerRadius = 10
        
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    
        //parentView.modalPresentationStyle = .Popover
    }
    
   
   
 
        func showAlert(message: String) {
       // self.titleLbl.text = title
        self.messageLbl.text = message
       UIApplication.shared.keyWindow?.addSubview(parentView)
            
    }
    
    
    
    @IBAction func onClickDone(_ sender: UIButton) {
        parentView.removeFromSuperview()
        delegate?.didTapOkButton(sender )
    }
    
    
    
    
    
    
    
}
